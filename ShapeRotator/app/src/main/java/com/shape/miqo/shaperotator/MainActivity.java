package com.shape.miqo.shaperotator;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {

    private float mStartX;
    private float mCurrentRotation;
    private View mImageView;
    private TextView mPercentTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FrameLayout frameLayout = findViewById(R.id.background_frame);
        mPercentTextView = findViewById(R.id.percentTextView);
        mImageView = findViewById(R.id.image_view);

        frameLayout.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(final View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mStartX = event.getX();
                break;

            case MotionEvent.ACTION_MOVE:
                float rotationDelta;

                rotationDelta = -(event.getX() - mStartX) / 10;
                mImageView.setRotation(mCurrentRotation + rotationDelta);
                mPercentTextView.setText(Float.toString(mImageView.getRotation()));

                break;

            case MotionEvent.ACTION_UP:
                mCurrentRotation = mImageView.getRotation();
                break;
        }

        return true;
    }
}
